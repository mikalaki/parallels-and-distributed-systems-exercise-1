# Parallels and Distributed systems exercise 1#
**!!DROPBOX link in THE REPORT IS BROKEN** 

**!!!!ΠΡΟΣΟΧΗ!!!!
Σχετικά με την υλοποίηση μας σε cilk ,σχετικά με το cilk_for που χρησιμοποιήσαμε 
στην λούπα η οποία "λαμβάνει " τα δεδομένα για το εσωτερικό και εξωτερικό υποδέντρο
(vptree_cilk.c ln.231 ),όπως γράφουμε στην αναφορά μας ήταν μια ριψοκίνδυνη τεχνική,
που ΠΟΤΕ δεν ενδείκνεται καθώς έχουμε σπάσιμο for loop σε race conditions(για τις
μεταβλητές outerCount και innerCount και,προβληματιστήκαμε αν θα την συμπεριλάβουμε
στα παραδοτέα μας,ωστόσο καθώς αυτή η αλλαγή επιτάχυνε κατά μεγάλο βαθμο
τα αποτελέσματα μας και πέρασε επιτυχώς απο τον ελεγκτή ορθότητας το αρχείο 
μας(τόσο τοπικά όσο και online) , ενώ επαληθεύτηκε ως σωστή και απο δικά μας 
τεστ σε μικρότερα δεδομένα (5-10 στοιχεία) αποφασίσαμε να την συμπεριλάβουμε στα 
παραδοτέα μας ,και επίσης δεν βρήκαμε καποιο τρόπο για την προσθήκη reducer σε silk σε γλώσσα c
(όλο τα υπαρχουσα online παραδείγματα  για reducer ήταν για c++)!!!
Στην συνέχεια όμως , σκεφτήκαμε ότι δεν είναι λογικό να αφήσουμε
"επικίνδυνο" κώδικα στο git του προτζεκτ, καθώς για μεγαλύτερα δεδομένα ή σε άλλα συστήματα
ενδέχεται η υλοποίηση να προκαλέσει σφάλματα , αποφασίσαμε να την αλλάξουμε 
και αντικαταστήσουμε το cilk_for (ln.231) με ένα απλό for( ληξηπρόθεσμα), πλέον η τυαχύτητα με 
την όποία τρέχει ο κώδικας του cilk  κυμένεται μεταξύ pthreads και openmp τείνοντας αρκετά προς 
αυτή της υλοποίησης με pthreads.**
 
Αυτή η μικροαλλαγή είναι και η μόνη διαφοροποίηση του πρώτου παραδοτέου
κώδικα(ο κωδικάς που στάλθηκε εντός προθεσμίας) με αυτόν 
που υπάρχει αυτή την στιγμή στο gitlab(πρώτος παραδοτέος κωδικας -> https://www.dropbox.com/s/6vtxxcg4b6n6gkw/code.tar.gz?dl=0) .
Καθώς αυτή η αλλαγη , επηρεάζει τους χρόνους 
εκτέλεσης της υλοποίησηας μας με cilk αποφασίσαμε να **παραθέσουμε ένα ξεχωριστό αρχείο pdf με τις μετρήσεις
του cilk μετά την αλλαγή, οι oποίες βρίσκονται στο repo στο αρχείο με όνομα 
cilkMeasurementsAfterChange.pdf (οι μετρήσεις για τις άλλες 3 υλοποιήσεις στην αρχική μας αναφορά
προφανώς ισχύουν!!!)**


**!!DROPBOX link in THE REPORT IS BROKEN!!**-->**DROPBOX active link **: https://www.dropbox.com/s/6vtxxcg4b6n6gkw/code.tar.gz?dl=0
(Πέρασε απο τον ελεγκτή ορθότητας)**(ΤΟ ΑΡΧΕΙΟ ΤΟΥ DROPBOX LINK ΔΕΝ ΠΕΡΙΕΧΕΙ ΤΗΝ ΑΛΛΑΓΗ ΠΟΥ ΈΓΙΝΕ 
ΣΤΗΝ ΥΛΟΠΟΊΗΣΗ ΜΕ CILK!!)**


Ευχαριστούμε για τον χρόνο σας!!

**WARNING!!!**
**
As it concerns the implementation of the cilk framework, more specifically the cilk_for that we used
in the loop which manages the data for the inner and the outer subtree(vptree_cilk.c ln.231),
we realized that we were parallelizing the loop that contains race conditions(for the variables innerCount,outerCount). 
This technique is not suggested and generally is considered reckless, dangerous and even fault. 
However as this change dramatically accelerated our ode and passed successfully through the validator, 
and our small data test (5-10 points) we decided not to change it. But after the end of deadline
we thought that this implementation was fault at all,cause it was very dangerous , and in bigger datasets and
other machines maybe it would create problems , so we decided just to do this minor 
change to our code.Now the execution time for cilk with this change is somewhere 
between openmp and pthreads. **
This minor change in ln.231 is the only differantion between the code we sent before
the deadline and the current code on gitlab (code we sent before the deadline ->https://www.dropbox.com/s/6vtxxcg4b6n6gkw/code.tar.gz?dl=0).
**We added new measurements about cilk 
in the cilkMeasurementsAfterChange.pdf file (the measurements for the  other 3 implementations
in the report are valid of course!!!)**


Thank you for your time.

Καρατζάς Μιχάλης ,9137, mikalaki@ece.auth.gr
Κυριάκος Μαραντίδης,9095